---
layout: default
title: Installation
---

# Install Coup de Grace

1. Download the [resource]().
1. Extract the source into your FiveM resources directory.
1. [Enable the resource](#enable-the-resource).

## Enable the Resource

The most common way of enablement is by the server.cfg:

```cfg
ensure phoenix-coup-de-grace
```
