---
layout: default
title: About
---

# Coup de Grace Resource

> Relieve your character of their suffering in various ways

# Configuration

You can configure two things:

1. The command that performs the Coup de Grace
1. The method of the Coup de Grace.

```lua
-- config.lua

Config.Command = "die"
Config.Method = "Knife"
```

## `Config.Command`

The command that is entered into chat to execute the Coup de Grace, with a preceding slash.

## `Config.Method`

The method must match any existing Table keys.

For example:

```lua
--- config.lua

Config.Method = "Drown" -- Matches Config.Methods.Drown
Config.Methods.Drown = {
  BeforeHook = function(ped)
    StartPlayerTeleport(ped, 24352, 55323, -1255, 0, true, false) -- put the player in the middle of the Alamo Sea
  end
}
```
