-- @copyright Phoenix Protocol - All Rights Reserved
-- @author sircapsalot
-- @since Jan 19, 2024

fx_version 'cerulean'
game 'gta5'
author 'Phoenix Protocol'
description 'Phoenix Coup de Grace'

client_script {
  'config.lua'
  'client.lua'
}
