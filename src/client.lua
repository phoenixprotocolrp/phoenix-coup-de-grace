-- @copyright Phoenix Protocol - All Rights Reserved
-- @author sircapsalot
-- @since Jan 19, 2024

RegisterCommand(Config.Command, function(source, _, _)
    local ped = PlayerPedId()

    if IsEntityDead(ped) then return end

    local method = Config.Methods[Config.Method]

    if method.AnimDict then -- if the method of coup de grace has an animation
      RequestAnimDict(Config.AnimDict)
      while not HasAnimDictLoaded(Config.AnimDict) do
          Citizen.Wait(0)
      end

      TaskPlayAnim(
        ped,
        Config.AnimDict,
        Config.AnimName,
        Config.AnimFlag,
        Config.AnimTime,
        Config.AnimTimeOut,
        Config.AnimTimeIn,
        0, 0, 0, 0
      )

      Citizen.Wait(Config.AnimTime + Config.AnimTimeOut + Config.AnimTimeIn)
    end

    if method.BeforeHook then method.BeforeHook(ped) end
    SetEntityHealth(ped, 0)
end, false)
