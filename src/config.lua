-- @copyright Phoenix Protocol
-- @author sircapsalot
-- @since Jan 19, 2024

Config = {}
Config.Command = "die" -- The command to execute the Coup de Grace (e.g. /die)
Config.Method = "Knife" -- The method to use for the Coup de Grace (Knife, Pistol, Explosion, etc.)

Config.Methods = {}
Config.Methods.Knife = {
  AnimDict = "melee@knife@streamed_core_fps",
  AnimName = "ground_attack_on_spot",
  AnimFlag = 1,
  AnimTime = 1000,
  AnimTimeOut = 1000,
  AnimTimeIn = 1000,
  BeforeHook = function(ped)
  end
}
Config.Methods.Pistol = {
  AnimDict = "melee@pistol@streamed_core_fps",
  AnimName = "ground_attack_on_spot",
  AnimFlag = 1,
  AnimTime = 1000,
  AnimTimeOut = 1000,
  AnimTimeIn = 1000,
  BeforeHook = function(ped)
  end
}
Config.Methods.Explosion = {
  BeforeHook = function(ped)
    AddExplosion(GetEntityCoords(ped), 0, 1.0, true, false, 1.0)
  end
}
Config.Methods.Puma = {
  BeforeHook = function(ped)
    local puma = GetHashKey("a_c_puma")
    RequestModel(puma)
    while not HasModelLoaded(puma) do
      Citizen.Wait(0)
    end
    local coords = GetEntityCoords(ped)
    local puma = CreatePed(28, puma, coords.x, coords.y, coords.z, 0, 1, 1)
    SetEntityAsMissionEntity(puma, true, true)
    SetEntityHealth(puma, 0)
  end
}
